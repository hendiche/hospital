<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Auth::routes();
Route::get('/', 'HomeController@index');

Route::get('customer/index', 'customercontroller@index')->name('customer/index');
    Route::post('customer/cfrm/{id}', 'customercontroller@add');
    Route::get('customer/edit/{id}', 'customercontroller@editForm');
    Route::post('customer/update/{id}', 'customercontroller@update');
    Route::get('customer/delete/{id}', 'customercontroller@delete');

Route::get('reception/index', 'receptioncontroller@index')->name('reception/index');
    Route::get('reception/checkin', 'receptioncontroller@checkin');
        Route::post('reception/checkin/cfrm', 'receptioncontroller@addCheckin');
        Route::get('reception/checkin/edit/{id}', 'receptioncontroller@editForm');
        Route::post('reception/checkin/update/{id}', 'receptioncontroller@update');
        Route::get('reception/checkin/delete/{id}', 'receptioncontroller@delete');

Route::get('receipt/index', 'receiptcontroller@index')->name('receipt/index');
    Route::get('receipt/receipt/{id}', 'receiptcontroller@receipt');
    Route::get('receipt/pay/{id}/{userid}', 'receiptcontroller@pay');
Route::get('prescription/index/{id}', 'receiptcontroller@showmedicine');
    Route::post('prescription/addtolist/{id}', 'receiptcontroller@prescription');
    Route::get('prescription/details/{id}', 'receiptcontroller@details');
    Route::get('prescription/cfrm/{id}', 'receiptcontroller@confirm');
    Route::get('prescription/delete/{id}/{receptionid}', 'receiptcontroller@delete');

Route::get('doctor/index', 'doctorcontroller@index')->name('doctor/index');
    Route::post('doctor/cfrm', 'doctorcontroller@add');
    Route::get('doctor/specialist', 'doctorcontroller@toSpecialist')->name('doctor/specialist');
        Route::post('doctor/specialist/cfrm', 'doctorcontroller@addSpecialist');

Route::get('room/index', 'roomcontroller@index')->name('room/index');
    Route::post('room/cfrm', 'roomcontroller@add');
    Route::get('room/type', 'roomcontroller@toType')->name('room/type');
        Route::post('room/type/cfrm', 'roomcontroller@addType');

Route::get('nurse/index', 'nursecontroller@index')->name('nurse/index');
    Route::post('nurse/cfrm', 'nursecontroller@add');

Route::get('medicine/index', 'medicinecontroller@index')->name('medicine/index');
    Route::post('medicine/cfrm', 'medicinecontroller@add');

Route::get('report', 'reportcontroller@index');

Route::get('user/index', 'addusercontroller@index')->name('user/index');;
    Route::post('user/cfrm', 'addusercontroller@add');
    Route::get('user/edit/{id}', 'addusercontroller@editForm');
    Route::post('user/update/{id}', 'addusercontroller@update');
    Route::get('user/delete/{id}', 'addusercontroller@delete');

Route::get('patient/index', 'patientcontroller@index');
    Route::get('patient/patient/{id}', 'patientcontroller@patient');
