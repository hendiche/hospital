<?php

use Illuminate\Database\Seeder;
use App\models\User;
use Illuminate\Support\Facades\Hash;


class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new user();

        $user->name = "admin";
        $user->email = "admin@admin.com";
        $user->password = hash::make("admin");
        $user->phone_num = "087894202996";
        $user->birth_date = "1996-04-16";
        $user->gender = "Male";
        $user->address = "paradise center";
        $user->role = 'Admin';

        $user->save();
    }
}
