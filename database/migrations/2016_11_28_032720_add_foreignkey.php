<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function ($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('rooms', function ($table) {
            $table->foreign('room_type_id')->references('id')->on('room_types');
        });

        Schema::table('doctors', function ($table) {
            $table->foreign('specialist_doctor_id')->references('id')->on('specialist_doctors');
        });

        Schema::table('receptions', function ($table) {
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('doctor_id')->references('id')->on('doctors');
            $table->foreign('room_id')->references('id')->on('rooms');
            $table->foreign('nurse_id')->references('id')->on('nurses');
        });

        Schema::table('receipts', function ($table) {
            $table->foreign('reception_id')->references('id')->on('receptions');
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('medicine_receipt', function ($table) {
            $table->foreign('medicine_id')->references('id')->on('medicines');
            $table->foreign('receipt_id')->references('id')->on('receipts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function ($table) {
            $table->dropForeign(['user_id']);
        });

        Schema::table('rooms', function ($table) {
            $table->dropForeign(['room_type_id']);
        });

        Schema::table('doctors', function ($table) {
            $table->dropForeign(['specialist_doctor_id']);
        });

        Schema::table('receptions', function ($table) {
            $table->dropForeign(['customer_id']);
            $table->dropForeign(['doctor_id']);
            $table->dropForeign(['room_id']);
            $table->dropForeign(['nurse_id']);
        });

        Schema::table('receipts', function ($table) {
            $table->dropForeign(['reception_id']);
            $table->dropForeign(['user_id']);
        });

        Schema::table('medicine_receipt', function ($table) {
            $table->dropForeign(['medicine_id']);
            $table->dropForeign(['receipt_id']);
        });
    }
}
