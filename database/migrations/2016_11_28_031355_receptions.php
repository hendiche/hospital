<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Receptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('doctor_id');
            $table->string('type_disease');
            $table->unsignedInteger('nurse_id')->nullable;
            $table->unsignedInteger('room_id')->nullable;
            $table->dateTime('date_in');
            $table->dateTime('date_out')->nullable();
            $table->decimal('checkup_fee', 20, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('receptions');
    }
}
