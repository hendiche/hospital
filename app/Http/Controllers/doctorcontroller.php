<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\doctor;
use App\models\specialist_doctor;

class doctorcontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $doctor = new doctor();
        $special = new specialist_doctor();
        return view('doctor/index')->with('specialist_doctors', $special->get())->with(
             'doctors',$doctor->join('specialist_doctors', 'doctors.specialist_doctor_id', '=', 'specialist_doctors.id')
            ->select('doctors.name', 'doctors.birth_date', 'doctors.gender', 'doctors.phone_num', 'doctors.address',
             'specialist_doctors.name as special')->get());
    }

    public function add(Request $request)
    {
        $name = $request->input('name');
        $bod = $request->input('BoD');
        $gender = $request->input('gender');
        $phone = $request->input('phonenum');
        $addres = $request->input('address');
        $specialist = $request->input('specialist');

        $doctor = new doctor();

        $doctor->name = $name;
        $doctor->birth_date = $bod;
        $doctor->gender = $gender;
        $doctor->phone_num = $phone;
        $doctor->address = $addres;
        $doctor->specialist_doctor_id = $specialist;
        $doctor->save();
        return Redirect()->route('doctor/index');
    }

    public function toSpecialist()
    {
        $special = new specialist_doctor();
        return view('doctor/specialist')->with('specialist_doctors', $special->get());
    }

    public function addSpecialist(Request $request)
    {
        $name = $request->input('name');

        $special = new specialist_doctor();

        $special->name = $name;
        $special->save();
        return Redirect()->route('doctor/specialist');
    }
}