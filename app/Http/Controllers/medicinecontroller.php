<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\medicine;

class medicinecontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $medicine = new medicine();
        return view('medicine/index')->with('medicines',$medicine->get());
    }

    public function add(Request $request)
    {
        $name = $request->input('name');
        $price = $request->input('price');

        $medicine = new medicine();

        $medicine->name = $name;
        $medicine->price = $price;
        $medicine->save();
        return Redirect()->route('medicine/index');
    }
}
