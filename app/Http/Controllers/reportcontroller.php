<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\receipt;
use App\models\reception;
use App\models\customer;
use App\models\doctor;
use App\models\nurse;
use App\models\room;
use App\models\User;

class reportcontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $receipt = new receipt();
        $reception = new reception();
        $customer = new customer();
        $doctor = new doctor();
        $nurse = new nurse();
        $room = new room();
        $user = new User();

        return view('report')->with('receipts', $receipt->leftJoin(
            'receptions', 'receptions.id', '=', 'receipts.reception_id')->leftJoin(
            'customers', 'customers.id', '=', 'receptions.customer_id')->leftJoin(
            'doctors', 'doctors.id', '=', 'receptions.doctor_id')->leftJoin(
            'nurses', 'nurses.id', '=', 'receptions.nurse_id')->leftJoin(
            'rooms', 'rooms.id', '=', 'receptions.room_id')->leftJoin(
            'users', 'users.id', '=', 'receipts.user_id')
            ->select('receipts.receipt_num', 'customers.name as customer', 'doctors.name as doctor', 'receptions.type_disease',
                'nurses.name as nurse', 'rooms.name as room', 'receptions.date_in', 'receptions.date_out', 'receipts.total',
                'users.name as user')->whereNotNull('users.name')->get());
    }
}
