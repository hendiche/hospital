<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\customer;
use App\models\doctor;
use App\models\room;
use App\models\nurse;
use App\models\reception;
use App\models\receipt;
use Carbon\carbon;

class receptioncontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $reception = new reception();
        $customer = new customer();
        $doctor = new doctor();
        $nurse = new nurse();
        $room = new room();
        return view('reception/index')->with('receptions', $reception->leftJoin(
             'customers', 'receptions.customer_id', '=', 'customers.id')->leftJoin(
             'doctors', 'receptions.doctor_id', '=', 'doctors.id')->leftJoin(
             'rooms', 'receptions.room_id', '=', 'rooms.id')->leftJoin(
             'nurses', 'receptions.nurse_id', '=', 'nurses.id')
             ->select('receptions.id', 'customers.name as customer', 'doctors.name as doctor', 'receptions.type_disease',
              'nurses.name as nurse', 'rooms.name as room', 'receptions.date_out')->whereNULL('receptions.date_out')->get());
    }

    public function checkin()
    {

        $reception = new reception();
        $customer = new customer();
        $doctor = new doctor();
        $nurse = new nurse();
        $room = new room();
        return view('reception/checkin')->with('receptions', $reception->leftJoin(
             'customers', 'receptions.customer_id', '=', 'customers.id')->leftJoin(
             'doctors', 'receptions.doctor_id', '=', 'doctors.id')->leftJoin(
             'rooms', 'receptions.room_id', '=', 'rooms.id')->leftJoin(
             'nurses', 'receptions.nurse_id', '=', 'nurses.id')
             ->select('receptions.id', 'customers.name as customer', 'doctors.name as doctor', 'receptions.type_disease',
               'nurses.name as nurse', 'rooms.name as room', 'receptions.date_in', 'receptions.date_out',
               'receptions.checkup_fee')->whereNull('receptions.date_out')->get())
             ->with('customers', $customer->get())->with('doctors', $doctor->get())
             ->with('rooms', $room->where('rooms.status', '=', 'avaiable')->get())->with('nurses', $nurse->get());
    }

    public function addCheckin(Request $request)
    {

        $customerid = $request->input('customer');
        $doctorid = $request->input('doctor');
        $disease = $request->input('disease');
        $nurseid = $request->input('nurse');
        $roomid = $request->input('room');
        $fee = $request->input('fee');

        $reception = new reception();
        $doctor = new doctor();
        $nurse = new nurse();
        $room = new room();
        $receipt = new receipt();

        if ($roomid != "") {
            $getroom = $room->where('rooms.id', '=', $roomid)->first();
            $roomqty = $getroom->amount_load;
            $newamount = $roomqty - 1;
            if ($newamount < 1) $roomstat = 'full';

            $getroom->amount_load = $newamount;
            if (isset($roomstat)) $getroom->status = $roomstat;
            $getroom->Save();

            $roomprice = $getroom->price;
        }

        $reception->customer_id = $customerid;
        $reception->doctor_id = $doctorid;
        $reception->type_disease = $disease;
        if ($nurseid != "") $reception->nurse_id = $nurseid;
        if ($roomid != "") $reception->room_id = $roomid;
        $reception->date_in = carbon::now();
        $reception->checkup_fee = $fee;
        $reception->save();

        // Create receipt -----------------------------------------------------------------------------------------------
        $getreception = $reception->where('receptions.customer_id', '=', $customerid)
        ->whereNULL('receptions.date_out')->where('receptions.type_disease', '=', $disease)->first();
        $numreceipt = strtotime($getreception->date_in);
        $receptionid = $getreception->id;
        if (isset($roomprice)) {
            $total = $getreception->checkup_fee + $roomprice;
        }
        else {
            $total = $getreception->checkup_fee;
        }

        $receipt->receipt_num = $numreceipt;
        $receipt->reception_id = $receptionid;
        $receipt->total = $total;
        $receipt->save();
        return Redirect()->route('reception/index');
    }

    public function editForm($id)
    {
        $reception = new reception();
        $customer = new customer();
        $doctor = new doctor();
        $nurse = new nurse();
        $room = new room();
        return view('reception/edit')->with('receptions', $reception::find($id))->with('customers', $customer->get())
         ->with('doctors', $doctor->get())->with('rooms', $room->where('rooms.status', '=', 'avaiable')->get())
         ->with('nurses', $nurse->get());
    }

    public function update(Request $request, $id)
    {
        $customerid = $request->input('customer');
        $doctorid = $request->input('doctor');
        $disease = $request->input('disease');
        $nurseid = $request->input('nurse');
        $roomid = $request->input('room');
        $fee = $request->input('fee');

        $reception = new reception();
        $customer = new customer();
        $doctor = new doctor();
        $nurse = new nurse();
        $room = new room();
        $edit = $reception::find($id);

        if ($roomid != "") {
            $getroom = $room->where('rooms.id', '=', $roomid)->first();
            $roomqty = $getroom->amount_load;
            $newamount = $roomqty - 1;
            if ($roomqty < 1) {
                $roomstat = 'full';
            }
        }

        $edit->customer_id = $customerid;
        $edit->doctor_id = $doctorid;
        $edit->type_disease = $disease;
        if ($nurseid != "") $edit->nurse_id = $nurseid;
        if ($roomid != "") $edit->room_id = $roomid;
        $edit->checkup_fee = $fee;
        $edit->save();

        if ($roomid != "") {
            $update = $room::find($roomid);
            $update->amount_load = $newamount;
            $update->status = $roomstat;
            $update->save();
        }
        return Redirect()->route('reception/index');
    }

    public function delete($id)
    {
        $reception = new reception();

        $del = $reception::find($id);
        $del->delete();
        return view('reception/index');
    }
}
