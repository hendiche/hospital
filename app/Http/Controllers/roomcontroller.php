<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\room;
use App\models\room_type;

class roomcontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $room = new room();
        $typeroom = new room_type();
        return view('room/index')->with('room_types', $typeroom->get())->with('rooms',$room->join(
             'room_types', 'rooms.room_type_id', '=', 'room_types.id')
            ->select('rooms.name', 'rooms.price', 'rooms.floor', 'rooms.amount_load', 'rooms.status',
             'room_types.name as room_type')->get());
    }

    public function add(Request $request)
    {
        $name = $request->input('name');
        $price = $request->input('price');
        $floor = $request->input('floor');
        $amountload = $request->input('amountload');
        $type = $request->input('roomtype');

        if ($amountload > 0) {
            $status = 'avaiable';
        }
        else {
            $status = 'full';
        }

        $room = new room();

        $room->name = $name;
        $room->price = $price;
        $room->floor = $floor;
        $room->amount_load = $amountload;
        $room->status = $status;
        $room->room_type_id = $type;
        $room->save();
        return Redirect()->route('room/index');
    }

    public function toType()
    {
        $typeroom = new room_type();
        return view('room/type')->with('room_types', $typeroom->get());
    }

    public function addType(Request $request)
    {
        $name = $request->input('name');

        $typeroom = new room_type();

        $typeroom->name = $name;
        $typeroom->save();
        return Redirect()->route('room/type');

    }
}
