<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\User;
use Illuminate\Support\Facades\Hash;

class addusercontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = new user();
        return view('user/index')->with('users',$user->get());
    }

    public function add(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $pass = $request->input('password');
        $phone = $request->input('phonenum');
        $bod = $request->input('BoD');
        $sex = $request->input('gender');
        $addres = $request->input('address');
        $role = $request->input('position');

        $user = new user();

        $user->name = $name;
        $user->email = $email;
        $user->password = hash::make($pass);
        $user->phone_num = $phone;
        $user->birth_date = $bod;
        $user->gender = $sex;
        $user->address = $addres;
        $user->role = $role;

        $user->save();
        return Redirect()->route('user/index');
    }

    public function editForm($id)
    {
        $user = new user();
        return view('user/edit')->with('users', $user::find($id));
    }

    public function update(Request $request, $id)
    {
        $user = new user();
        $edit = $user::find($id);

        $edit->name = $request->input('name');
        $edit->email = $request->input('email');
        $edit->password = hash::make($request->input('newpassword'));
        $edit->phone_num = $request->input('phonenum');
        $edit->birth_date = $request->input('BoD');
        $edit->gender = $request->input('gender');
        $edit->address = $request->input('address');
        $edit->role = $request->input('position');

        $edit->save();
        return Redirect()->route('user/index');
    }

    public function delete($id)
    {
        $user = new user();
        $del = $user::find($id);
        $del->delete();
        return Redirect()->route('user/index');
    }
}
