<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\User;
use App\models\customer;


class customercontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $customer = new customer();
        $user = new user();
        return view('customer/index')->with('customers',$customer->join('users', 'customers.user_id', '=', 'users.id')
            ->select('customers.id', 'customers.IC_num', 'customers.name', 'customers.birth_date', 'customers.gender',
                'customers.phone_num', 'customers.religion', 'customers.address', 'users.name as nameuser')
            ->orderBy('customers.id', 'asc')->get());
    }

    public function add(Request $request, $id)
    {
        $icnum = $request->input('icnum');
        $name = $request->input('name');
        $bod = $request->input('BoD');
        $gender = $request->input('gender');
        $phone = $request->input('phonenum');
        $religion = $request->input('religion');
        $addres = $request->input('address');

        $customer = new customer();

        $customer->IC_num = $icnum;
        $customer->name = $name;
        $customer->birth_date = $bod;
        $customer->gender = $gender;
        $customer->phone_num = $phone;
        $customer->religion = $religion;
        $customer->address = $addres;
        $customer->user_id = $id;

        $customer->save();
        return Redirect()->route('customer/index');
    }

    public function editForm($id)
    {
        $customer = new customer();
        return view('/customer/edit')->with('customers', $customer::find($id));
    }

    public function update(Request $request, $id)
    {
        $customer = new customer();
        $edit = $customer::find($id);

        $edit->IC_num = $request->input('icnum');
        $edit->name = $request->input('name');
        $edit->birth_date = $request->input('BoD');
        $edit->gender = $request->input('gender');
        $edit->phone_num = $request->input('phonenum');
        $edit->religion = $request->input('religion');
        $edit->address = $request->input('address');

        $edit->save();
        return Redirect()->route('customer/index');
    }

    public function delete($id)
    {
        $customer = new customer();
        $del = $customer::find($id);
        $del->delete();
        return Redirect()->route('customer/index');
    }
}