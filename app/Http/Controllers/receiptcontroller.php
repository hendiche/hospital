<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\receipt;
use App\models\reception;
use App\models\medicine;
use App\models\customer;
use App\models\doctor;
use App\models\nurse;
use App\models\room;
use Carbon\carbon;
use Session;

class receiptcontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $receipt = new receipt();
        $reception = new reception();
        $customer = new customer();
        return view('receipt/index')->with('receipts', $receipt->leftJoin(
            'receptions', 'receipts.reception_id', '=', 'receptions.id')->Join(
            'customers', 'receptions.customer_id', '=', 'customers.id')
            ->select('receipts.id', 'receipts.receipt_num', 'customers.name as customer_name', 'receipts.total', 'receipts.user_id')
            ->whereNULL('receptions.date_out')->get());
    }

    public function receipt($id)
    {
        $receipt = receipt::find($id);
        $reception = reception::where('id', $receipt->reception_id)->first();
        $customer = customer::where('id', $reception->customer_id)->first();
        $doctor = doctor::where('id', $reception->doctor_id)->first();
        $nurse = nurse::where('id', $reception->nurse_id)->first();
        $room = room::where('id', $reception->room_id)->first();
        $medicine = new medicine();

        return view('receipt/receipt')->with('receipts', $receipt)
        ->with('receptions', $reception)
        ->with('customers', $customer)
        ->with('tgl', carbon::now()->format('Y-m-d'))
        ->with('doctors', $doctor)
        ->with('nurses', $nurse)
        ->with('rooms', $room)
        ->with('medicines', $medicine->get());
    }

    public function pay($id, $userid)
    {
        $receipt = receipt::find($id);
        $reception = reception::where('id', $receipt->reception_id)->first();
        $room = room::where('id', $reception->room_id)->first();

        if (isset($room)) {
            $roomqty = $room->amount_load;
            $newamount = $roomqty + 1;
            if ($newamount >0 ) $roomstat = 'avaiable';

            $room->amount_load = $newamount;
            $room->status = $roomstat;
            $room->save();
        }

        $reception->date_out = carbon::now();
        $reception->Save();

        $receipt->user_id = $userid;
        $receipt->save();
        return Redirect()->route('receipt/index');

    }

    public function showmedicine($id)
    {
        $reception = reception::find($id);
        $medicine = new medicine();
        return view('medicine/index')->with('receptions', $reception)->with('medicines',$medicine->get());
    }

    public function prescription(Request $request, $id)
    {
        $medicine = $request->input('medicine');
        $qty = $request->input('qty');
        $bool = false;

        $medicine = medicine::find($medicine);

        $getprice = $medicine->price;
        $amount = $getprice * $qty;

        $array = [
            'id' => $medicine->id,
            'name' => $medicine->name,
            'price' => $getprice,
            'quantity' => $qty,
            'amount' => $amount,
            'reception_id' => $id
        ];

        $getprescription = $request->session()->get('myprescription');

        for ($i = 0; $i < count($getprescription); $i++) {
            if (!empty($getprescription) && $getprescription[$i]['id'] == $array['id'] AND $getprescription[$i]['reception_id'] == $array['reception_id']) {
                $bool = true;
                $getprescription[$i]['quantity'] += $array['quantity'];
                $getprescription[$i]['amount'] += $array['amount'];
                break;
            }
            else {
                $bool = false;
            }
        }

        if ($bool == true) {
            Session::put('myprescription', $getprescription);
        }
        else {
            Session::push('myprescription', $array);
        }
        return Redirect('prescription/index/'.$id);
    }

    public function details(Request $request, $id)
    {
        $request->session()->forget('mydetails');
        $bool = true;
        $reception = reception::find($id);
        $getprescription = $request->session()->get('myprescription');
        $details = $request->session()->get('mydetails');
        for ($i = 0; $i < count($getprescription); $i++) {
            if ($getprescription[$i]['reception_id'] == $id) {
                for ($j = 0; $j < count($details); $j++) {
                    if (!empty($details) AND $details[$j]['reception_id'] == $id) {
                        Session::put('mydetails', [$details[$j]]);
                    }
                    else {
                        Session::push('mydetails', $getprescription[$i]);
                    }
                }
                if (count($details) == 0) Session::push('mydetails', $getprescription[$i]);
            }
        }
        return view ('receipt/details')->with('receptions', $reception);
    }

    public function confirm(Request $request, $id)
    {
        $reception = reception::find($id);
        $receipt = new receipt();
        $getreceipt = $receipt->where('reception_id', $reception->id)->first();
        $getprice = $getreceipt->total;
        $getdetails = $request->session()->get('mydetails');

        foreach ($getdetails as $data) {
            $getprice += $data['amount'];
        }

        $getreceipt->total = $getprice;
        $getreceipt->save();

        foreach ($getdetails as $data) {
            $receipt = $receipt::find($getreceipt->id);
            $receipt->medicines()->attach($data['id'], ['quantity' => $data['quantity'], 'amount' => $data['amount'] ]);
        }

        $getprescription = $request->session()->get('myprescription');
        $count = count($getprescription);
        for ($i = 0; $i < $count; $i++) {
            if ($getprescription[$i]['reception_id'] == $id) {
                unset($getprescription[$i]);
            }
        }
        $sort = array_values($getprescription);
        Session::put('myprescription', $sort);

        return Redirect()->route('reception/index');
    }

    public function delete(Request $request, $id, $receptionid)
    {
        $list = $request->session()->get('myprescription');
        for ($i = 0; $i < count($list); $i++) {
            if ($list[$i]['id'] == $id AND $list[$i]['reception_id'] == $receptionid) {
                unset($list[$i]);
                break;
            }
        }
        $list = array_values($list);
        Session::put('myprescription', $list);
        return Redirect('prescription/details/'.$receptionid);
    }
}