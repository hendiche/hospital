<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\room;
use App\models\room_type;
use App\models\reception;
use App\models\customer;
use App\models\User;

class patientcontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $room = new room();
        $type = new room_type();
        return view('patient/index')->with('rooms', $room->leftjoin(
            'room_types', 'room_types.id', '=', 'rooms.room_type_id')
            ->select('rooms.id', 'rooms.name', 'rooms.floor', 'rooms.amount_load', 'rooms.status', 'room_types.name as type')->get());
    }

    public function patient($id)
    {
        $reception = new reception();
        $customer = new customer();
        $user = new user();

        return view('patient/patient')->with('receptions', $reception->join(
            'customers', 'customers.id', '=', 'receptions.customer_id')->join(
            'users', 'users.id', '=', 'customers.user_id')
            ->select('customers.name', 'customers.birth_date', 'customers.gender', 'customers.religion', 'customers.address',
                'receptions.date_in', 'receptions.date_out', 'users.name as staff')->where('receptions.room_id', '=', $id)
            ->whereNull('receptions.date_out')->get());
    }
}
