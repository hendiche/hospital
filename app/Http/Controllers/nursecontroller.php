<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\nurse;

class nursecontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $nurse = new nurse();
        return view('nurse/index')->with('nurses',$nurse->get());
    }

    public function add(Request $request)
    {
        $name = $request->input('name');
        $bod = $request->input('BoD');
        $gender = $request->input('gender');
        $phone = $request->input('phonenum');
        $addres = $request->input('address');

        $nurse = new nurse();

        $nurse->name = $name;
        $nurse->birth_date = $bod;
        $nurse->gender = $gender;
        $nurse->phone_num = $phone;
        $nurse->address = $addres;

        $nurse->save();
        return Redirect()->route('nurse/index');
    }
}
