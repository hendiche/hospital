<?php

namespace App\models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class customer extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'birth_date', 'gender', 'phone_num', 'religion', 'address',
    ];

    protected $guarded = [
        'IC_num', 'user_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
    ];

    public function user()
    {
        return $this->belongsTo('App\models\User');
    }

    public function reception()
    {
        return $this->hasMany('App\models\reception');
    }
}
