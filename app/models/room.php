<?php

namespace App\models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class room extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'floor', 'amount_load', 'status',
    ];

    protected $guarded = [
        'room_type_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
    ];

    public function roomType()
    {
        return $this->belongsTo('App\models\room_type');
    }

    public function reception()
    {
        return $this->hasMany('App\models\reception');
    }
}
