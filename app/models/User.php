<?php

namespace App\models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'password', 'phone_num', 'birth_date', 'gender', 'address', 'role',
    ];

    protected $guarded = [
        'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'id',
    ];

    public function customer()
    {
        return $this->hasMany('App\models\customer');
    }

    public function receipt()
    {
        return $this->hasMany('App\models\receipt');
    }
}
