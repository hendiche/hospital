<?php

namespace App\models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class reception extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_disease', 'date_in', 'date_out', 'checkup_fee',
    ];

    protected $guarded = [
        'customer_id', 'doctor_id', 'nurse_id', 'room_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
    ];

    public function doctor()
    {
        return $this->belongsTo('App\models\doctor');
    }

    public function room()
    {
        return $this->belongsTo('App\models\room');
    }

    public function customer()
    {
        return $this->belongsTo('App\models\customer');
    }

    public function nurse()
    {
        return $this->belongsTo('App\models\nurse');
    }

    public function receipt()
    {
        return $this->hasMany('App\models\receipt');
    }
}
