<?php

namespace App\models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class receipt extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'receipt_num', 'total'
    ];

    protected $guarded = [
        'reception_id', 'user_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
    ];

    public function medicines()
    {
        return $this->belongsToMany('App\models\medicine')->withPivot('quantity', 'amount')->orderBy('medicine_receipt.id', 'asc');
    }

    public function user()
    {
        return $this->belongsTo('App\models\user');
    }

    public function reception()
    {
        return $this->belongsTo('App\models\reception');
    }
}
