<?php

namespace App\models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class doctor extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'birth_date', 'gender', 'phone_num', 'address',
    ];

    protected $guarded = [
        'specialist_doctor_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
    ];

    public function specialistDoctor()
    {
        return $this->belongsTo('App\models\specialist_doctor');
    }

    public function reception()
    {
        return $this->hasMany('App\models\reception');
    }
}
