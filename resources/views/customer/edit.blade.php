@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->

</head>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('/') }}">{{ Html::image('../../../images/logo.png', 'imglogo', ['class' => 'imglogopanelhead']) }}</a>
                <span class="fontpanelheading">Edit Customer</span>
            </div>
            <div class="panel-body">
                {!! Form::open(['url' => 'customer/update/'.$customers->id, 'method' => 'post']) !!}
                    <table class="table">
                        <tr>
                            <td> {{ Form::label('icnum', 'IC Number') }} </td>
                            <td> {{ Form::text('icnum', $customers->IC_num, ['required' => '']) }} </td>
                        </tr>
                        <tr>
                            <td> {{ Form::label('name', 'Name') }} </td>
                            <td> {{ Form::text('name', $customers->name, ['required' => '', 'minlength' => '3']) }} </td>
                        </tr>
                        <tr>
                            <td> {{ Form::label('BoD', 'Birth of Date') }} </td>
                            <td> {{ Form::date('BoD', $customers->birth_date) }} </td>
                        </tr>
                        <tr>
                            <td> {{ Form::label('gender', 'Gender') }} </td>
                            <td> {{ Form::radio('gender', 'Male', true) }}Male &nbsp;
                                 {{ Form::radio('gender', 'Female') }}Female
                            </td>
                        </tr>
                        <tr>
                            <td> {{ Form::label('phonenum', 'Phone Number') }} </td>
                            <td> {{ Form::text('phonenum', $customers->phone_num, ['required' => '']) }}</td>
                        </tr>
                        <tr>
                            <td> {{ Form::label('religion', 'Religion')}} </td>
                            <td> {{ Form::radio('religion', 'Islam') }}Islam &nbsp;
                                 {{ Form::radio('religion', 'Protestantism') }}Protestantism &nbsp;
                                 {{ Form::radio('religion', 'Catholicism') }}Catholicism &nbsp;
                                 {{ Form::radio('religion', 'Hinduism') }}Hinduism &nbsp;
                                 {{ Form::radio('religion', 'Buddhism', true) }}Buddhism &nbsp;
                                 {{ Form::radio('religion', 'Confucianism') }}Confucianism
                            </td>
                        </tr>
                        <tr>
                            <td> {{ Form::label('address', 'Address') }} </td>
                            <td> {{ Form::textarea('address', $customers->address, ['class' => 'addressmaxarea']) }} </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                {{ Form::submit('confirm', ['class' => 'btn btn-info']) }}
                                <a href="{{ url('/customer/index') }}" class="btn btn-warning">Back</a>
                            </td>
                        </tr>
                    </table>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</body>
</html>
@endsection