@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->

</head>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('/') }}">{{ Html::image('../../../images/logo.png', 'imglogo', ['class' => 'imglogopanelhead']) }}</a>
                <span class="fontpanelheading">Customer</span>
            </div>
            <div class="panel-body">
                <table class="table" id="myTable">
                    <thead>
                        <tr>
                            <td>IC Number</td>
                            <td>Name</td>
                            <td>Birth of Date</td>
                            <td>Gender</td>
                            <td>Phone Number</td>
                            <td>Religion</td>
                            <td>Address</td>
                            <td>Staff Name</td>
                        @if (Auth::user()->role == 'Admin')
                            <td>Option</td>
                        @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($customers as $table)
                        <tr>
                            <td> {{ $table->IC_num }} </td>
                            <td> {{ $table->name }} </td>
                            <td> {{ $table->birth_date }} </td>
                            <td> {{ $table->gender }} </td>
                            <td> {{ $table->phone_num }} </td>
                            <td> {{ $table->religion }} </td>
                            <td> {{ $table->address }} </td>
                            <td> {{ $table->nameuser }} </td>
                        @if (Auth::user()->role == 'Admin')
                            <td> <a href="{{ url('/customer/edit/'.$table->id) }}" class="btn btn-info">Edit</a>
                             <a href="{{ url('/customer/delete/'.$table->id) }}" class="btn btn-danger">Delete</a> </td>
                        @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Add</button>
                <a href="{{ url('/') }}" class="btn btn-warning">Back</a>
                {{-- modal --}}
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Customer</h4>
                            </div>
                            <div class="modal-body">
                                {!! Form::open(['url' => 'customer/cfrm/'.Auth::user()->id, 'method' => 'post']) !!}
                                    <table class="table nomarginbottom">
                                        <tr>
                                            <td> {{ Form::label('icnum', 'IC Number') }} </td>
                                            <td> {{ Form::text('icnum', null, ['required' => '']) }} </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('name', 'Name') }} </td>
                                            <td> {{ Form::text('name', null, ['required' => '', 'minlength' => '3']) }} </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('BoD', 'Birth of Date') }} </td>
                                            <td> {{ Form::date('BoD') }} </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('gender', 'Gender') }} </td>
                                            <td> {{ Form::radio('gender', 'Male', true) }}Male &nbsp;
                                                 {{ Form::radio('gender', 'Female') }}Female
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('phonenum', 'Phone Number') }} </td>
                                            <td> {{ Form::text('phonenum', null, array('required' => '')) }}</td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('religion', 'Religion')}} </td>
                                            <td> {{ Form::radio('religion', 'Islam') }}Islam &nbsp;
                                                 {{ Form::radio('religion', 'Protestantism') }}Protestantism &nbsp;
                                                 {{ Form::radio('religion', 'Catholicism') }}Catholicism &nbsp;
                                                 {{ Form::radio('religion', 'Hinduism') }}Hinduism &nbsp;
                                                 {{ Form::radio('religion', 'Buddhism', true) }}Buddhism &nbsp;
                                                 {{ Form::radio('religion', 'Confucianism') }}Confucianism
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('address', 'Address') }} </td>
                                            <td> {{ Form::textarea('address', null, ['class' => 'addressmaxarea']) }} </td>
                                        </tr>
                                    </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {{ Form::submit('confirm', ['class' => 'btn btn-primary']) }}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
@endsection