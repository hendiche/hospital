@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->

</head>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('/') }}">{{ Html::image('../../../images/logo.png', 'imglogo', ['class' => 'imglogopanelhead']) }}</a>
                <span class="fontpanelheading">Report</span>
            </div>
            <div class="panel-body">
                <table class="table" id="myTable">
                    <thead>
                        <tr>
                            <td>Receipt number</td>
                            <td>Customer Name</td>
                            <td>Doctor Name</td>
                            <td>Type Disease</td>
                            <td>Nurse Name</td>
                            <td>Room</td>
                            <td>Date In</td>
                            <td>Date Out</td>
                            <td>Total Price</td>
                            <td>Staff Handle</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($receipts as $table)
                        <tr>
                            <td> {{ $table->receipt_num }} </td>
                            <td> {{ $table->customer }} </td>
                            <td> {{ $table->doctor }} </td>
                            <td> {{ $table->type_disease }} </td>
                            <td> {{ $table->nurse }} @if (is_null($table->nurse)) No Nurse @endif </td>
                            <td> {{ $table->room }} @if (is_null($table->room)) No Room @endif </td>
                            <td> {{ $table->date_in }} </td>
                            <td> {{ $table->date_out }} </td>
                            <td> Rp.{{ number_format($table->total, 2, '.', ',') }} </td>
                            <td> {{ $table->user }} </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <a href="{{ url('/') }}" class="btn btn-warning">Back</a>
            </div>
        </div>
    </div>
</body>
</html>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
@endsection