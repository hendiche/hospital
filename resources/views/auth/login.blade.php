@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 login-bgimg">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 login-midalignlogo">
                    {{ Html::image('../../../images/logo.png', 'imglogo', ['class' => 'login-imglogo']) }}
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 login-img425">
                    <div>{{ Html::image('../../../images/login.png', 'imglogin', ['class' => 'login-imglogin']) }}</div>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label login-lefttxt">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control login-inptbox" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label login-lefttxt">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control login-inptbox" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panel-footer login-footer">&copy;Copyrights 2016, Rumah Sakit Dream Smart
            </div>
        </div>
    </div>
</div>
@endsection
