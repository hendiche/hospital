@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->

</head>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('/') }}">{{ Html::image('../../../images/logo.png', 'imglogo', ['class' => 'imglogopanelhead']) }}</a>
                <span class="fontpanelheading">Checkin</span>
            </div>
            <div class="panel-body">
            @if (Auth::user()->role == 'Admin')
                <table class="table" id="myTable">
                    <thead>
                        <tr>
                            <td>Customer Name</td>
                            <td>Doctor</td>
                            <td>Disease Type</td>
                            <td>Nurse</td>
                            <td>Room</td>
                            <td>Date in</td>
                            <td>Checkup Fee</td>
                            <td>Option</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($receptions as $table)
                        <tr>
                            <td> {{ $table->customer }} </td>
                            <td> {{ $table->doctor }} </td>
                            <td> {{ $table->type_disease }} </td>
                            <td> {{ $table->nurse }} @if (is_null($table->nurse)) No Nurse @endif </td>
                            <td> {{ $table->room }} @if (is_null($table->room)) No Room @endif </td>
                            <td> {{ $table->date_in }} </td>
                            <td> Rp.{{ number_format($table->checkup_fee, 2, '.', ',') }} </td>
                            <td> <a href="{{ url('reception/checkin/edit/'.$table->id) }}" class="btn btn-info">Edit</a>
                                 <a href="{{ url('reception/checkin/delete/'.$table->id) }}" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Add</button>
                <a href="{{ url('/reception/index') }}" class="btn btn-warning">Back</a>
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Reception</h4>
                            </div>
                            <div class="modal-body">
                                {!! Form::open(['url' => 'reception/checkin/cfrm', 'method' => 'post']) !!}
                                    <table class="table">
                                        <tr>
                                            <td> {{ Form::label('customer', 'Customer') }} </td>
                                            <td> <select name="customer" required>
                                                    <option></option>
                                                 @foreach ($customers as $list)
                                                    <option value="{{ $list->id }}">{{ $list->IC_num }} - {{ $list->name }}</option>
                                                 @endforeach
                                                 </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('doctor', 'Doctor') }} </td>
                                            <td> <select name="doctor" required>
                                                    <option></option>
                                                 @foreach ($doctors as $list)
                                                    <option value="{{ $list->id }}">{{ $list->name }}</option>
                                                 @endforeach
                                                 </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('disease', 'Disease') }} </td>
                                            <td> {{ Form::text('disease', null, ['required' => '']) }} </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('nurse', 'Nurse') }} </td>
                                            <td> <select name="nurse">
                                                    <option></option>
                                                 @foreach ($nurses as $list)
                                                    <option value="{{ $list->id }}">{{ $list->name }}</option>
                                                 @endforeach
                                                 </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('room', 'Room') }} </td>
                                            <td> <select name="room">
                                                    <option></option>
                                                 @foreach ($rooms as $list)
                                                    <option value="{{ $list->id }}">{{ $list->name }}</option>
                                                 @endforeach
                                                 </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('fee', 'Checkup Fee') }} </td>
                                            <td> {{ Form::number('fee', null, ['required' => '']) }} </td>
                                        </tr>
                                    </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {{ Form::submit('confirm', ['class' => 'btn btn-primary']) }}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            @elseif (Auth::user()->role == 'Reception')
                {!! Form::open(['url' => 'reception/checkin/cfrm', 'method' => 'post']) !!}
                    <table class="table">
                        <tr>
                            <td> {{ Form::label('customer', 'Customer') }} </td>
                            <td> <select name="customer" required>
                                    <option></option>
                                 @foreach ($customers as $list)
                                    <option value="{{ $list->id }}">{{ $list->IC_num }} - {{ $list->name }}</option>
                                 @endforeach
                                 </select>
                            </td>
                        </tr>
                        <tr>
                            <td> {{ Form::label('doctor', 'Doctor') }} </td>
                            <td> <select name="doctor" required>
                                    <option></option>
                                 @foreach ($doctors as $list)
                                    <option value="{{ $list->id }}">{{ $list->name }}</option>
                                 @endforeach
                                 </select>
                            </td>
                        </tr>
                        <tr>
                            <td> {{ Form::label('disease', 'Disease') }} </td>
                            <td> {{ Form::text('disease', null, ['required' => '']) }} </td>
                        </tr>
                        <tr>
                            <td> {{ Form::label('nurse', 'Nurse') }} </td>
                            <td> <select name="nurse">
                                    <option></option>
                                 @foreach ($nurses as $list)
                                    <option value="{{ $list->id }}">{{ $list->name }}</option>
                                 @endforeach
                                 </select>
                            </td>
                        </tr>
                        <tr>
                            <td> {{ Form::label('room', 'Room') }} </td>
                            <td> <select name="room">
                                    <option></option>
                                 @foreach ($rooms as $list)
                                    <option value="{{ $list->id }}">{{ $list->name }}</option>
                                 @endforeach
                                 </select>
                            </td>
                        </tr>
                        <tr>
                            <td> {{ Form::label('fee', 'Checkup Fee') }} </td>
                            <td> {{ Form::number('fee', null, ['required' => '']) }} </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                {{ Form::submit('Confirm', ['class' => 'btn btn-primary']) }}
                                <a href="{{ url('/reception/index') }}" class="btn btn-warning">Back</a>
                            </td>
                        </tr>
                    </table>
                {!! Form::close() !!}
             @endif
            </div>
        </div>
    </div>
</body>
</html>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
@endsection