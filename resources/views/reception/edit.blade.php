@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->

</head>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('/') }}">{{ Html::image('../../../images/logo.png', 'imglogo', array('class' => 'imglogopanelhead')) }}</a>
                <span class="fontpanelheading">Edit Reception</span>
            </div>
            <div class="panel-body">
                {!! Form::open(['url' => 'reception/checkin/update/'.$receptions->id, 'method' => 'post']) !!}
                    <table class="table">
                        <tr>
                            <td> {{ Form::label('customer', 'Customer Name') }} </td>
                            <td> <select name="customer" required>
                                 @foreach ($customers as $list)
                                    <option value="{{ $list->id }}" @if ($list->id == $receptions->customer_id) selected @endif>{{ $list->IC_num }} - {{ $list->name }}</option>
                                 @endforeach
                                 </select>
                            </td>
                        </tr>
                        <tr>
                            <td> {{ Form::label('doctor', 'Doctor') }} </td>
                            <td> <select name="doctor" required="">
                                 @foreach ($doctors as $list)
                                    <option value="{{ $list->id }}" @if ($list->id == $receptions->doctor_id) selected @endif >{{ $list->name }}</option>
                                 @endforeach
                                 </select>
                            </td>
                        </tr>
                        <tr>
                            <td> {{ Form::label('disease', 'Disease') }} </td>
                            <td> {{ Form::text('disease', $receptions->type_disease, ['required' => '']) }} </td>
                        </tr>
                        <tr>
                            <td> {{ Form::label('nurse', 'Nurse') }} </td>
                            <td> <select name="nurse">
                                 <option></option>
                                 @foreach ($nurses as $list)
                                    <option value="{{ $list->id }}" @if ($list->id == $receptions->nurse_id) selected @endif>{{ $list->name }}</option>
                                 @endforeach
                                 </select>
                            </td>
                        </tr>
                        <tr>
                            <td> {{ Form::label('room', 'Room') }} </td>
                            <td> <select name="room">
                                 <option></option>
                                 @foreach ($rooms as $list)
                                    <option value="{{ $list->id }}" @if ($list->id == $receptions->room_id) selected @endif>{{ $list->name }}</option>
                                 @endforeach
                                 </select>
                            </td>
                        </tr>
                        <tr>
                            <td> {{ Form::label('fee', 'Checkup Fee') }} </td>
                            <td> {{ Form::number('fee', $receptions->checkup_fee, ['required' => '']) }} </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                {{ Form::submit('confirm', ['class' => 'btn btn-info']) }}
                                <a href="{{ url('/reception/checkin') }}" class="btn btn-warning">Back</a>
                            </td>
                        </tr>
                    </table>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</body>
</html>
@endsection