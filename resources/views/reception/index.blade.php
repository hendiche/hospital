@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->

</head>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('/') }}">{{ Html::image('../../../images/logo.png', 'imglogo', ['class' => 'imglogopanelhead']) }}</a>
                <span class="fontpanelheading">Reception</span>
            </div>
            <div class="panel-body">
            @if (Auth::user()->role == 'Admin' OR Auth::user()->role == 'Reception')
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2 col-sm-offset-2  menu-midmenu">
                        <div class="well well-sm">
                            <a href="{{ url('reception/checkin') }}">
                                {{ HTML::image("../../../images/in.png", "checkin", ['class' => 'menu-submenu']) }}<br>
                                <span class="submenufont">Checkin</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2 col-sm-offset-2 menu-midmenu">
                        <div class="well well-sm">
                            <a href="{{ url('receipt/index') }}">
                                {{ HTML::image("../../../images/out.png", "checkin", ['class' => 'menu-submenu']) }}<br>
                                <span class="submenufont">Checkout</span>
                            </a>
                        </div>
                    </div>
                </div>
            @elseif (Auth::user()->role == 'Doctor')
                <table class="table" id="myTable">
                    <thead>
                        <tr>
                            <td>Customer Name</td>
                            <td>Doctor</td>
                            <td>Disease Type</td>
                            <td>Nurse</td>
                            <td>Room</td>
                            <td>Option</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($receptions as $table)
                            <tr>
                                <td> {{ $table->customer }} </td>
                                <td> {{ $table->doctor }} </td>
                                <td> {{ $table->type_disease }} </td>
                                <td> {{ $table->nurse }} @if (is_null($table->nurse)) No Nurse @endif </td>
                                <td> {{ $table->room }} @if (is_null($table->room)) No Room @endif </td>
                                <td>
                                    <a href="{{ url('prescription/index/'.$table->id) }}" class="btn btn-info @if ($table->doctor != Auth::user()->name) disabled @endif">Give medicine</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
            </div>
            <div class="panel-footer footer">
                <a href="{{ url('/') }}" class="btn btn-warning">Back</a>
            </div>
        </div>
    </div>
</body>
</html>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#myTable').DataTable( {
        "columns": [
         null,
         null,
         null,
         null,
         null,
         { "width": "10%" }
        ]
    });
});
</script>
@endsection