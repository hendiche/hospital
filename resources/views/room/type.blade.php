@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->

</head>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('/') }}">{{ Html::image('../../../images/logo.png', 'imglogo', ['class' => 'imglogopanelhead']) }}</a>
                <span class="fontpanelheading">Type Room</span>
            </div>
            <div class="panel-body">
                <table class="table" id="myTable">
                    <thead>
                        <tr>
                            <td>Name</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($room_types as $table)
                        <tr>
                            <td> {{ $table->name }} </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Add</button>
                <a href="{{ url('/room/index') }}" class="btn btn-warning">Back</a>
                {{-- modal --}}
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Room Type</h4>
                            </div>
                            <div class="modal-body">
                                {!! Form::open(['url' => 'room/type/cfrm', 'method' => 'post']) !!}
                                    <table class="table nomarginbottom">
                                        <tr>
                                            <td> {{ Form::label('name', 'Name') }} </td>
                                            <td> {{ Form::text('name', null, ['required' => '', 'minlength' => '3']) }} </td>
                                        </tr>
                                    </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {{ Form::submit('confirm', ['class' => 'btn btn-primary']) }}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
@endsection