@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->

</head>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('/') }}">{{ Html::image('../../../images/logo.png', 'imglogo', ['class' => 'imglogopanelhead']) }}</a>
                <span class="fontpanelheading">Room</span>
            </div>
            <div class="panel-body">
                <table class="table" id="myTable">
                    <thead>
                        <tr>
                            <td>Name</td>
                            <td>Price</td>
                            <td>Floor</td>
                            <td>Amount Load</td>
                            <td>Status</td>
                            <td>Type Room</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($rooms as $table)
                        <tr>
                            <td> {{ $table->name }} </td>
                            <td> Rp.{{ number_format($table->price, 2, '.', ',') }} </td>
                            <td> {{ $table->floor }} </td>
                            <td> {{ $table->amount_load }} </td>
                            <td> {{ $table->status }} </td>
                            <td> {{ $table->room_type }} </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{-- modal --}}
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Room</h4>
                            </div>
                            <div class="modal-body">
                                {!! Form::open(['url' => 'room/cfrm', 'method' => 'post']) !!}
                                    <table class="table nomarginbottom">
                                        <tr>
                                            <td> {{ Form::label('name', 'Name') }} </td>
                                            <td> {{ Form::text('name', null, ['required' => '', 'minlength' => '3']) }} </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('price', 'Price') }} </td>
                                            <td> {{ Form::number('price', null, ['required' => '']) }} </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('floor', 'Floor') }} </td>
                                            <td> {{ Form::text('floor', null, ['required' => '']) }} </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('amountload', 'Amount Load') }} </td>
                                            <td> {{ Form::number('amountload', null, ['required' => '']) }} </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('roomtype', 'Room Type') }} </td>
                                            <td> <select name="roomtype" required>
                                                 @foreach($room_types as $list)
                                                    <option value="{{ $list->id }}">{{ $list->name }}</option>
                                                 @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {{ Form::submit('confirm', ['class' => 'btn btn-primary']) }}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer footer">
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Add</button>
                <a href="{{ url('/') }}" class="btn btn-warning">Back</a>
                <a href="{{ url('/room/type') }}" class="pull-right btn btn-info">Type Room</a>
            </div>
        </div>
    </div>
</body>
</html>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
@endsection