@extends('layouts.app')

@section('content')
{{-- $name = {{ Auth::user()->name }}; --}}
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ Html::image('../../../images/logo.png', 'imglogo', ['class' => 'imglogopanelhead']) }}
                    <span class="fontpanelheading">Main Menu</span>
                </div>
                <div class="panel-body">
                @if (Auth::user()->role == 'Admin')
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12 menu-midmenu">
                            <div class="well well-sm">
                                <a href="{{ url('/customer/index') }}">
                                    {{ HTML::image("../../../images/customer.png", "customer", ['class' => 'menu-submenu']) }}<br>
                                    <span class="submenufont">Customer</span>
                                </a>
                            </div>

                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 menu-midmenu">
                            <div class="well well-sm">
                                <a href="{{ url('/reception/index') }}">
                                    {{ HTML::image("../../../images/reception.png", "reception", ['class' => 'menu-submenu']) }}<br>
                                    <span class="submenufont">Reception</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 menu-midmenu">
                            <div class="well well-sm">
                                <a href="{{ url('/doctor/index') }}">
                                    {{ HTML::image("../../../images/doctor.png", "doctor", ['class' => 'menu-submenu']) }}<br>
                                    <span class="submenufont">Doctor</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 menu-midmenu">
                            <div class="well well-sm">
                                <a href="{{ url('/room/index') }}">
                                    {{ HTML::image("../../../images/room.png", "room", ['class' => 'menu-submenu']) }}<br>
                                    <span class="submenufont">Room</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 menu-midmenu">
                            <div class="well well-sm">
                                <a href="{{ url('/nurse/index') }}">
                                    {{ HTML::image("../../../images/nurse.png", "nurse", ['class' => 'menu-submenu']) }}<br>
                                    <span class="submenufont">Nurse</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 menu-midmenu">
                            <div class="well well-sm">
                                <a href="{{ url('/medicine/index') }}">
                                    {{ HTML::image("../../../images/medicine.png", "medicine", ['class' => 'menu-submenu']) }}<br>
                                    <span class="submenufont">Medicine</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 menu-midmenu">
                            <div class="well well-sm">
                                <a href="{{ url('/report') }}">
                                    {{ HTML::image("../../../images/report.png", "report", ['class' => 'menu-submenu']) }}<br>
                                    <span class="submenufont">Report</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 menu-midmenu">
                            <div class="well well-sm">
                                <a href="{{ url('/user/index') }}">
                                    {{ HTML::image("../../../images/adduser.png", "adduser", ['class' => 'menu-submenu']) }}<br>
                                    <span class="submenufont">User</span>
                                </a>
                            </div>
                        </div>
                    </div>
                @elseif (Auth::user()->role == 'Reception')
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12 menu-midmenu">
                            <div class="well well-sm">
                                <a href="{{ url('/customer/index') }}">
                                    {{ HTML::image("../../../images/customer.png", "customer", ['class' => 'menu-submenu']) }}<br>
                                    <span class="submenufont">Customer</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 menu-midmenu">
                            <div class="well well-sm">
                                <a href="{{ url('/reception/index') }}">
                                    {{ HTML::image("../../../images/reception.png", "reception", ['class' => 'menu-submenu']) }}<br>
                                    <span class="submenufont">Reception</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 menu-midmenu">
                            <div class="well well-sm">
                                <a href="{{ url('/patient/index') }}">
                                    {{ HTML::image("../../../images/Showpatientperroom.png", "showpatient", ['class' => 'menu-submenu']) }}<br>
                                    <span class="submenufont">Search Patient</span>
                                </a>
                            </div>
                        </div>
                    </div>
                @elseif (Auth::user()->role == 'Doctor')
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-4 col-sm-offset-4 menu-midmenu">
                            <div class="well well-sm">
                                <a href="{{ url('/reception/index') }}">
                                    {{ HTML::image("../../../images/patient.png", "reception", ['class' => 'menu-submenu']) }}<br>
                                    <span class="submenufont">Patient</span>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
