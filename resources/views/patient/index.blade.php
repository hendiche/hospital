@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->

</head>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('/') }}">{{ Html::image('../../../images/logo.png', 'imglogo', ['class' => 'imglogopanelhead']) }}</a>
                <span class="fontpanelheading">Patient Room</span>
            </div>
            <div class="panel-body">
                <div class="row">
                @foreach ($rooms as $view)
                <a href="{{ url('patient/patient/'.$view->id) }}" style="color: black;">
                    <div class="col-md-6 col-sm-6 col-xs-12 menu-midmenu well well-sm @if ($view->status == "avaiable")
                                                                                bggreen
                                                                              @else
                                                                                bgred
                                                                              @endif " style="text-align: left;">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12"> {{-- row pertama (room) --}}
                                <div class="row">
                                    <div class="col-md-6 col-sm-6"> Room </div>
                                    <div class="col-md-6 col-sm-6">: {{ $view->name }} </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12"> {{-- row kedua (floor) --}}
                                <div class="row">
                                    <div class="col-md-6 col-sm-6"> Floor </div>
                                    <div class="col-md-6 col-sm-6">: {{ $view->floor }} </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12"> {{-- row ketiga (type) --}}
                                <div class="row">
                                    <div class="col-md-6 col-sm-6"> Room Type </div>
                                    <div class="col-md-6 col-sm-6">: {{ $view->type }} </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3"> Amount Left </div>
                                    <div class="col-md-3 col-sm-3">: {{ $view->amount_load }} </div>
                                    <div class="col-md-3 col-sm-3"> Status </div>
                                    <div class="col-md-3 col-sm-3">: {{ $view->status }} </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                @endforeach
                </div>
            </div>
            <div class="panel-footer footer">
                <a href="{{ url('/') }}" class="btn btn-warning">Back</a>
            </div>
        </div>
    </div>
</body>
</html>
@endsection