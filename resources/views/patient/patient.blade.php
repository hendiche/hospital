@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->

</head>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('/') }}">{{ Html::image('../../../images/logo.png', 'imglogo', ['class' => 'imglogopanelhead']) }}</a>
                <span class="fontpanelheading">Patient Room</span>
            </div>
            <div class="panel-body">
                <div class="row">
                @if (empty($receptions))
                    <div style="font-size: 20px">No Patient in This Room</div>
                @else
                @foreach ($receptions as $view)
                    <div class="col-md-6 col-sm-6 col-xs-12 menu-midmenu well well-sm bgcard">
                        <div class="row nomarginRandL">
                            <div class="col-md-12 col-sm-12"> {{-- row pertama (header) --}}
                                <div class="row nomarginRandL">
                                    <div class="col-md-2 col-sm-2"> {{ HTML::image('../../../images/logo.png', 'imglogosmall', ['class' => 'iconcard']) }} </div>
                                    <div class="col-md-8 col-sm-8" style="text-align: left; padding-left: 35px;font-size: 20px">DreamSmart Hospital</div>
                                    <div class="col-md-2 col-sm-2"> {{ HTML::image('../../../images/iconhospital.png', 'imgicon', ['class' => 'iconcard']) }}  </div>
                                </div>
                            </div>
                            <div>&nbsp;</div>
                            <div class="col-md-12 col-sm-12" style="text-align: left;"> {{-- row kedua (name) --}}
                                <div class="row nomarginRandL">
                                    <div class="col-md-6 col-sm-6">Name</div>
                                    <div class="col-md-6 col-sm-6">: {{ $view->name }} </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12" style="text-align: left;"> {{-- row ketiga (date) --}}
                                <div class="row nomarginRandL">
                                    <div class="col-md-6 col-sm-6">Birth Date</div>
                                    <div class="col-md-6 col-sm-6">: {{ $view->birth_date }} </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12" style="text-align: left;"> {{-- row keempat (gender) --}}
                                <div class="row nomarginRandL">
                                    <div class="col-md-6 col-sm-6">Gender</div>
                                    <div class="col-md-6 col-sm-6">: {{ $view->gender }} </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12" style="text-align: left;"> {{-- row kelima (religion) --}}
                                <div class="row nomarginRandL">
                                    <div class="col-md-6 col-sm-6">Religion</div>
                                    <div class="col-md-6 col-sm-6">: {{ $view->religion }} </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12" style="text-align: left;"> {{-- row keenam (gender) --}}
                                <div class="row nomarginRandL">
                                    <div class="col-md-6 col-sm-6">Address</div>
                                    <div class="col-md-6 col-sm-6">: {{ $view->address }} </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12" style="text-align: left;"> {{-- row ketuju (datein and user) --}}
                                <div class="row nomarginRandL">
                                    <div class="col-md-2 col-sm-2">Date</div>
                                    <div class="col-md-4 col-sm-4">: {{ Carbon\carbon::parse($view->date_in)->format('Y-m-d') }} </div>
                                    <div class="col-md-3 col-sm-3">Reception</div>
                                    <div class="col-md-3 col-sm-3">: {{ $view->staff }} </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                @endif
                </div>
            </div>
            <div class="panel-footer footer">
                <a href="{{ url('/patient/index') }}" class="btn btn-warning">Back</a>
            </div>
        </div>
    </div>
</body>
</html>
@endsection