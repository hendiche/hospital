@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->

</head>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('/') }}">{{ Html::image('../../../images/logo.png', 'imglogo', ['class' => 'imglogopanelhead']) }}</a>
                <span class="fontpanelheading">Medical Prescription</span>
            </div>
            <div class="panel-body">
                <table class="table nomarginbottom">
                {{-- {{dd(Session::get('myprescription'))}} --}}
                    @if (empty(Session::get('mydetails')))
                        <div>No Medicine Were Added</div>
                    @else
                        <tr>
                            <td>Name</td>
                            <td>Price</td>
                            <td>Quantity</td>
                            <td>Amount</td>
                            <td>Option</td>
                        </tr>
                        @foreach (Session::get('mydetails') as $key => $list)
                            <tr>
                                <td> {{ $list['name'] }} </td>
                                <td> {{ number_format($list['price'], 2, '.', ',') }} </td>
                                <td> {{ $list['quantity'] }} </td>
                                <td> {{ number_format($list['amount'], 2, '.', ',') }} </td>
                                <td><a href="{{ url('prescription/delete/'.$list['id'].'/'.$receptions->id) }}" class="btn btn-danger">Remove</a></td>
                            </tr>
                        @endforeach
                    @endif
                </table>
            </div>
            <div class="panel-footer footer">
                <a href="{{ url('/prescription/cfrm/'.$receptions->id) }}" class="btn btn-primary">Confirm</a>
                <a href="{{ url('/prescription/index/'.$receptions->id) }}" class="btn btn-warning">Back</a>
            </div>
        </div>
    </div>
</body>
</html>
@endsection