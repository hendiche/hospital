@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->

</head>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('/') }}">{{ Html::image('../../../images/logo.png', 'imglogo', ['class' => 'imglogopanelhead']) }}</a>
                <span class="fontpanelheading">Checkout</span>
            </div>
            <div class="panel-body">
                <table class="table" id="myTable">
                    <thead>
                        <tr>
                            <td>Receipt number</td>
                            <td>Customer Name</td>
                            <td>Total Price</td>
                            <td>option</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($receipts as $table)
                        <tr>
                            <td> {{ $table->receipt_num }} </td>
                            <td> {{ $table->customer_name }} </td>
                            <td> Rp.{{ number_format($table->total, 2, '.', ',') }} </td>
                            <td> @if (is_null($table->user_id))
                                <a href="{{ url('receipt/receipt/'.$table->id) }}" class="btn btn-info">payment</a> @endif
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <a href="{{ url('reception/index') }}" class="btn btn-warning">Back</a>
            </div>
        </div>
    </div>
</body>
</html>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
@endsection