@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->

</head>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('/') }}">{{ Html::image('../../../images/logo.png', 'imglogo', ['class' => 'imglogopanelhead']) }}</a>
                <span class="fontpanelheading">Payment</span>
            </div>
            <div class="panel-body">
                <div class="panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table">
                                    <tr>
                                        <td> Receipt Number </td>
                                        <td>: {{ $receipts->receipt_num }} </td>
                                    </tr>
                                    <tr>
                                        <td> Patient Name </td>
                                        <td>: {{ ucfirst($customers->name) }} </td>
                                    </tr>
                                    <tr>
                                        <td> Date </td>
                                        <td>: {{ $tgl }} </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table">
                                    <tr>
                                        <td> Doctor Name </td>
                                        <td>: {{ ucfirst($doctors->name) }} </td>
                                    </tr>
                                    <tr>
                                        <td> Nurse Name </td>
                                        <td>:@if (is_null($nurses)) -- @else {{ ucfirst($nurses->name) }} @endif </td>
                                    </tr>
                                    <tr>
                                        <td> Room </td>
                                        <td>:@if (is_null($rooms)) -- @else {{ $rooms->name }} @endif </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                    {{-- {{ dd($medicines) }} --}}
                         <div>
                            <table class="table nomarginbottom">
                                <tr>
                                    <td>Descripsion</td>
                                    <td>price</td>
                                    <td>qty</td>
                                    <td>total</td>
                                </tr>
                                <tr>
                                    <td>checkup free (include doctor and nurse)</td>
                                    <td> Rp.{{ number_format($receptions->checkup_fee, 2, '.', ',') }} </td>
                                    <td>1</td>
                                    <td> Rp.{{ number_format($receptions->checkup_fee,2 , '.', ',') }} </td>
                                </tr>
                            @if (isset($rooms))
                                <tr>
                                   <td>Room Price</td>
                                   <td> Rp.{{ number_format($rooms->price, 2, '.', ',') }} </td>
                                   <td>1</td>
                                   <td> Rp.{{ number_format($rooms->price,2 ,'.', ',') }} </td>
                                </tr>
                            @endif
                            @foreach ($receipts->medicines as $list)
                                <tr>
                                    <td>
                                        @foreach ($medicines as $getmedicname)
                                            @if ($getmedicname->id == $list->id)
                                                {{ $getmedicname->name }}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach ($medicines as $getmedicprice)
                                            @if ($getmedicprice->id == $list->id)
                                                Rp.{{ number_format($getmedicprice->price,2 ,'.' ,',') }}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td> {{ $list->pivot->quantity }} </td>
                                    <td> Rp.{{ number_format($list->pivot->amount,2 ,'.', ',') }} </td>
                                </tr>
                            @endforeach
                                <tr style="font-weight: bold;">
                                    <td colspan="2"></td>
                                    <td>Total</td>
                                    <td> Rp.{{ number_format($receipts->total, 2, '.', ',') }} </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                    <div class="panel-footer footer">
                        <a href="{{ url('receipt/pay/'.$receipts->id.'/'.Auth::user()->id) }}" class="btn btn-primary">Pay</a>
                        <a href="{{ url('/receipt/index') }}" class="btn btn-warning">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
@endsection