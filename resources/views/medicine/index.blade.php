@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->

</head>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('/') }}">{{ Html::image('../../../images/logo.png', 'imglogo', ['class' => 'imglogopanelhead']) }}</a>
                <span class="fontpanelheading">Medicine</span>
            </div>
            <div class="panel-body">
                <table class="table" id="myTable">
                    <thead>
                        <tr>
                            <td>Name</td>
                            <td>Price</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($medicines as $table)
                        <tr>
                            <td> {{ $table->name }} </td>
                            <td> Rp.{{ number_format($table->price,2 ,'.', ',') }} </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="panel-footer footer">
            @if (Auth::user()->role == 'Doctor')
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myPrescription">Add</button>
                <a href="{{ url('/prescription/details/'.$receptions->id) }}" class="btn btn-info pull-right">Details</a>
                <a href="{{ url('/reception/index') }}" class="btn btn-warning">Back</a>
                {{-- modal prescription --}}
                <div class="modal fade" id="myPrescription" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Give Medicine</h4>
                            </div>
                            <div class="modal-body">
                                {!! Form::open(['url' => 'prescription/addtolist/'.$receptions->id, 'method' => 'post']) !!}
                                    <table class="table nomarginbottom">
                                    <tr>
                                        <td> {{ Form::label('name', 'Name') }} </td>
                                        <td> <select name="medicine" required>
                                            <option></option>
                                            @foreach ($medicines as $list)
                                                <option value="{{ $list->id }}">{{ $list->name }}</option>
                                            @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> {{ Form::label('qty', 'Qty') }} </td>
                                        <td> {{ Form::number('qty', null, ['require' => '']) }} </td>
                                    </tr>
                                    </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {{ Form::submit('confirm', ['class' => 'btn btn-primary']) }}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end modal prescription --}}
            @else
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Add</button>
                <a href="{{ url('/') }}" class="btn btn-warning">Back</a>
                {{-- modal --}}
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Medicine</h4>
                            </div>
                            <div class="modal-body">
                                {!! Form::open(['url' => 'medicine/cfrm', 'method' => 'post']) !!}
                                    <table class="table nomarginbottom">
                                        <tr>
                                            <td> {{ Form::label('name', 'Name') }} </td>
                                            <td> {{ Form::text('name', null, ['required' => '']) }} </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('price', 'Price') }} </td>
                                            <td> {{ Form::number('price', null, ['require' => '']) }} </td>
                                        </tr>
                                    </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {{ Form::submit('confirm', ['class' => 'btn btn-primary']) }}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end modal (div atas) --}}
            @endif
            </div>
        </div>
    </div>
</body>
</html>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
@endsection