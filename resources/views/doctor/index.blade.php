@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->

</head>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('/') }}">{{ Html::image('../../../images/logo.png', 'imglogo', array('class' => 'imglogopanelhead')) }}</a>
                <span class="fontpanelheading">Doctor</span>
            </div>
            <div class="panel-body">
                <table class="table" id="myTable">
                    <thead>
                        <tr>
                            <td>Name</td>
                            <td>Birth of Date</td>
                            <td>Gender</td>
                            <td>Phone Number</td>
                            <td>Address</td>
                            <td>Specialist</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($doctors as $table)
                        <tr>
                            <td> {{ $table->name }} </td>
                            <td> {{ $table->birth_date }} </td>
                            <td> {{ $table->gender }} </td>
                            <td> {{ $table->phone_num }} </td>
                            <td> {{ $table->address }} </td>
                            <td> {{ $table->special }} </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{-- modal --}}
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Doctor</h4>
                            </div>
                            <div class="modal-body">
                                {!! Form::open(['url' => 'doctor/cfrm', 'method' => 'post']) !!}
                                    <table class="table nomarginbottom">
                                        <tr>
                                            <td> {{ Form::label('name', 'Name') }} </td>
                                            <td> {{ Form::text('name', null, ['required' => '', 'minlength' => '3']) }} </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('BoD', 'Birth of Date') }} </td>
                                            <td> {{ Form::date('BoD') }} </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('gender', 'Gender') }} </td>
                                            <td> {{ Form::radio('gender', 'Male', true) }}Male &nbsp;
                                                 {{ Form::radio('gender', 'Female') }}Female
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('phonenum', 'Phone Number') }} </td>
                                            <td> {{ Form::text('phonenum', null, ['required' => '']) }}</td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('address', 'Address') }} </td>
                                            <td> {{ Form::textarea('address', null, ['class' => 'addressmaxarea']) }} </td>
                                        </tr>
                                        <tr>
                                            <td> {{ Form::label('specialist', 'Specialist') }} </td>
                                            <td> <select name="specialist" required>
                                                @foreach($specialist_doctors as $list)
                                                    <option value="{{ $list->id }}">{{ $list->name }}</option>
                                                @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {{ Form::submit('confirm', ['class' => 'btn btn-primary']) }}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer footer">
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Add</button>
                <a href="{{ url('/') }}" class="btn btn-warning">Back</a>
                <a href="{{ url('/doctor/specialist') }}" class="pull-right btn btn-info">Specialist Doctor</a>
            </div>
        </div>
    </div>
</body>
</html>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
@endsection